export class Course {
    public name: string;
    public code: string;
    public info: boolean;
    public description?: string;
    public images?: any[];
    public pdf?: string;
}
