export class Project {
    public name: string;
    public customer: string;
    public info?: boolean;
    public images?: any[];
    public pdf?: string;
    public url?: string;
    public description?:string;
}
