import { Component, OnInit } from '@angular/core';
import { CourseDialogComponent } from '../../app/dialogs/course-dialog/course-dialog.component';
// Model
import { Course } from './../../models/course.model';
import { MatDialog } from '@angular/material';


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {


  private courses: Course[];
  private activeCourse: Course;
  private overlay: false;

  constructor(private matDialog: MatDialog) { }

  ngOnInit() {
    this.initCourses();
   
  }

  initCourses(): void {
    
    this.courses = [
      {
        name: 'Webbteknik 1',
        code: '1ME321',
        info: false,
      },
      {
        name: 'Studier i medieteknik',
        code: '1ME301',
        info: false
      },
      {
        name: 'Digitala medier',
        code: '1ME311',
        info: false
      },
      {
        name: 'Webbteknik 2',
        code: '1ME322',
        info: false
      },
      {
        name: 'Webbteknik 3',
        code: '1ME323',
        info: false
      },
      {
        name: 'Interaktionsdesign 1',
        code: '1ME331',
        info: false
      },
      {
        name: 'Ekonomi för webbutvecklare',
        code: '1FE052',
        info: false
      },
      {
        name: 'Juridik för webbutvecklare',
        code: '1RV041',
        info: false
      },
      {
        name: 'Projektkurs i medieteknik 1',
        code: '1ME302',
        info: true,
        description: 'Detta projekt gick ut på att tillämpa det vi lärt oss i tidigare kurser under det första året. \n Temat var turism i Småland och på Öland. Vi blev indelade i grupper och fick en målgrupp tilldelade oss, sedan fick vi i uppgift att skapa en passande applikation.\n Gruppen jag tillhörde blev tilldelade målgruppen medelålders golfintresserade par, och utvecklade därefter Golfguiden. En webbplats med information om Småland och Ölands golfbanor och kringliggande relevanta punkter såsom boende, restauranger och nöje. Denna information hämtades från ett REST-API skapat för kursen, som gav oss tillgång till den data som användes i projektet.\nJag stod för webbplatsens logik där användande av Google Maps API spelade en central roll för att presentera information.',
        images: [ 
          { src: 'projektkurs1.png' }
        ],
        pdf: '1ME302_Grupp4_U3.pdf'
      },
      {
        name: 'Webbteknik 4',
        code: '1ME324',
        info: true,
        description: 'I denna kursuppgift skapade jag en webbtjänst i vilken användare kan skapa artiklar utifrån tre olika kategorier. Artiklarna publiceras sedan på utgångssidan ordnade efter det datum de publicerades, senaste först. \n Den fiktiva tjänsten ger registrerade användare möjlighet att skapa nya och redigera befintliga artiklar i ett slags bloggformat. \n Webbtjänsten var php-driven och krävde inloggning för att komma åt författarverktyget för artiklarna.' ,
        images: [ 
          { src: 'webb4.png' }
        ],
        pdf: '1ME324.pdf'
      },
      {
        name: 'Interaktionsdesign 2',
        code: '1ME332',
        info: false
      },
      {
        name: 'Webbteknik 5',
        code: '1ME325',
        info: true,
        description: 'I denna kurs gick kursuppgiften ut på att skapa en tärningsapplikation.\nAnvändare kan lägga till och ta bort tärningar. Tärningarna kan rullas alla samtidigt, eller en i taget. Uppgiftens syfte var att använda sig av objektorienterad JavaScript.',
        images: [ 
          { src: 'webb5.jpg' }
        ],
        pdf: 'Michaela_Olausson_1ME325.pdf'
      },
      {
        name: 'Databasteori',
        code: '1DV513',
        info: true,
        description: 'Denna kursuppgift handlade om att planera och skapa en databas, för att använda sig av den genom en applikation av något slag. Jag valde att skapa en databas för ett kortspel. Arkham Horror - the Card Game. \n Idén var att skapa en tjänst där användare kan leta efter specifika kort. Arkham Horror - the Card Game är ett kortspel som bygger på samarbete, där två eller flera spelare ska samla ihop ledtrådar för att klara spelet. \n Produkten blev en php-driven webbplats som dynamiskt presenterade kort beroende på vilka val som gjordes i menyerna.\n Tjänsten byggdes för personer som redan äger spelet men som behöver en plats där de snabbt kan få en överblick över tillgängliga kort, utan att behöva ta fram den fysiska kortleken.\nTjänsten hämtar data ifrån en NoSQL-databas (MongoDB). Just en databas av den typen lämpade sig mycket väl i detta sammanhang, då formen för ett dokument inte är lika fast som en rad i en SQL-databas. Något som passade helt in i kortens struktur, där även information mellan kort inom samma typ kan avvika från varandra.',
        images: [ 
          { src: 'databasteori.png' }
        ],
        pdf: 'Michaela_Olausson_1DV513_Assignment2_part2.pdf'
      },
      {
        name: 'Webbteknik 6',
        code: '1ME326',
        info: true,
        description: 'I denna kurs valde jag som kursuppgift att göra om en tidigare uppgift, nämligen kursuppgiften i databasteori.\nDet jag gjorde var att separera tjänsten i två delar. Från att ha varit ett "block" blev det istället en tydlig klientdel (skalapplikation) och en serverdel (REST-API + databas).\nKlientdelen hämtar informationen för korten via nämda API som i sin tur skötte frågorna till databashanteraren.',
        images: [
          { src: 'databasteori.png' }
        ],
        pdf: 'Michaela_Olausson_1ME326_Kursuppgift.pdf'
      },
      {
        name: 'Interaktionsdesign 3',
        code: '1ME333',
        info: false
      },
      {
        name: 'Projektledning och entreprenörskap',
        code: '1FE058',
        info: false
      },
      {
        name: 'Projektkurs 2 i medieteknik',
        code: '1ME303',
        info: true,
        description: 'I Projektkurs 2 skapade jag och min studentkollega Petra, ett spel.\nSpelet heter Robo Maze och går ut på att hålla sig vid liv i en labyrint så länge som möjligt. Man håller roboten vid liv genom att plocka upp batterier.',
        images: [
          { src: 'posterförslag4.1.png' }
        ],
        pdf: '1ME303_MicOla&PetLei_U2.pdf',
      },
      {
        name: 'Datorteknik - introduktion med projektarbete',
        code: '1DT100',
        info: true,
        description: 'Programmering av Arduino, grundläggande elkretsteori, datorns olika beståndsdelar, datakommunikation etc.\n I denna kurs projektarbete skapade jag en liten moodlight vars färger var tänkt att motsvara den omgivande temperaturen. Utomhustemperaturen hämtades via enhetens inbyggda WiFi och ett öppet väder-API. Temperaturen närmast enheten mättes av genom en temperatursensor.',
        images: [
          { src:'datorteknik.jpg' }
        ],
        pdf: 'Michaela_Olausson_Projektrapport_1DT100_HT17.pdf'
      },
      {
        name: 'Vetenskaplig metod i medieteknik',
        code: '2ME301',
        info: false
      },
      {
        name: 'Verksamhetsförlagd praktik i medieteknik',
        code: '2ME303',
        info: false
      },
      {
        name: 'Examensarbete i medieteknik',
        code: '2ME30E',
        info: true,
        description: 'Min uppsats handlar om hur ett GDPR-kompatibelt grafiskt användargränssnitt kan förhöja en användarens känsla av kontroll, gällande dennes personuppgifter. Klicka på länken "läs tillhörande rapport" för att läsa uppsatsen.',
        images: [
          { src:'uppsats_1.png' }
        ],
        pdf: 'Michaela_Olausson_uppsats.pdf'
      },
      {
        name: 'Mobile Games and Entertainment',
        code: '4ME308',
        info: true,
        description: 'I denna kurs var uppgiften att skapa en prototyp av ett s.k. "serious game", vilket är en typ av spel som inte har som huvudsyfte att underhålla spelaren. Syftet med spelet kan istället vara att få fram ett budskap eller lära ut något. Min spelkoncept handlar om att skapa medvetenhet kring tjuvjakt på noshörningar. Spelet går ut på att du som parkvakt ska hålla undan tjuvjägare för att hålla parkens invånare vid liv. I rapporten jag skrivit om spelet finns en utförligare förklaring samt en länk till prototypen.',
        images: [
          { src:'onecrossonerhino.png' }
        ],
        pdf: '4ME308_VT18_Michaela_Olausson_Project_Report.pdf'
      },
      {
        name: 'Forskningsutmaningar i medieteknik',
        code: '2ME302',
        info: false
      }
    ];

    this.sortCourses();
  }

  sortCourses(): void {
    this.courses.sort(function(a, b){
      if ( a.name < b.name ) { return -1; }
      if ( a.name > b.name ) { return 1; }
      return 0;
    });
  }

  activate( course ): void {
    let name = course.name;
    let code = course.code;
    let pdf = course.pdf;
    let images = course.images;
    let description = course.description;
    let dialogRef
      dialogRef = this.matDialog.open(CourseDialogComponent, {
        data: {
          name: name,
          code: code,
          pdf: pdf,
          images: images,
          description: description
        }
    });
  }
}
