import { Component, OnInit } from '@angular/core';
// Model
import { Project } from './../../models/project.model';
import { ProjectDialogComponent } from '../../app/dialogs/project-dialog/project-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  private projects: Project[];

  constructor(private matDialog: MatDialog) { }

  ngOnInit() {
    this.initProjects();
  }

  initProjects(): void {
    this.projects = [
      {
        name: 'Hållbarhetskollen',
        customer: 'Norconsult',
        url: 'https://hallbarhetskollen.norconsult.se/',
        info: true,
        images: [
          { src: 'hallbarhetskollen1.png' },
          { src: 'hallbarhetskollen2.png' }
        ],
        description: "Hållbarhetskollen är ett interaktivt verktyg som skall hjälpa oss och vår kund att väcka tankar som kan leda till ökad hållbarhet i alla typer av projekt. Verktyget uppmanar oss att formulera förbättringsförslag i uppdragsstart för en preliminär bedömning. Ta hjälp av dina kollegor och av vår innovationsmetodik. Uppdragsledaren tar förbättringsförslagen till Beställaren som då kan få en bättre bild av projektets potential när det gäller hållbarhet. - Norconsult"
      }
    ];
  }
  activate(project) {
    let name = project.name;
    let customer = project.customer;
    let url = project.url;
    let images = project.images;
    let description = project.description;
    console.log(description)
    let dialogRef
      dialogRef = this.matDialog.open(ProjectDialogComponent, {
        data: {
          name: name,
          customer: customer,
          url: url,
          images: images,
          description: description
        }
      });
  }
}
