import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  private phonenumber = '076 30 56 004';
  private email = 'michaelaolausson(at)gmail.com';
  private address = 'Växjö';
  private civic_number = '1988-04-26';
  private linked_in = "https://www.linkedin.com/in/michaela-olausson";

  constructor() { }

  ngOnInit() {
  }

}
