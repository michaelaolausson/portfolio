import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employments',
  templateUrl: './employments.component.html',
  styleUrls: ['./employments.component.css']
})
export class EmploymentsComponent {
  private employments:any[];

  constructor() { 
  }

  ngOnInit() {
    this.initEmployments();
  }
  initEmployments() {
    this.employments = [
      {
        employer: "Virserums Konsthall",
        position: "Receptionist",
        start: new Date(1304899200*1000),
        end: new Date(1322352000*1000),
        start_string: "2011-05-09",
        end_string: "2011-11-27",
        description: "Receptionist och cafébiträde.",
        type: "Deltid"
      },
      {
        employer: "Allelektronik AB",
        position: "Montör",
        start: new Date(1322697600*1000),
        end: new Date(1362009600*1000),
        start_string: "2011-12-01",
        end_string: "2013-02-28",
        description: "Montering och packning av elektronikprodukter.",
        type: "Heltid"
      },
      {
        employer: "Järnforsens Plåtslageri AB",
        position: "Maskinoperatör",
        start: new Date(1362096000*1000),
        end: new Date(1440028800*1000),
        start_string: "2013-03-01",
        end_string: "2015-08-20",
        description: "Ansvarig för drift och körning av företagets vattenskärningsmaskin." ,
        type: "Heltid"
      },
      {
        employer: "Linnéuniversitetet",
        position: "Amanuens",
        start: new Date(1441065600*1000),
        end: null,
        start_string: "2015-09-01",
        end_string: null,
        description: "Bland annat examinator av uppgifter, handledare samt extralärare i grundkurserna i webbteknik.",
        type: "Halvtid"
      }
    ]
    this.employments.sort(function(a,b){
      // Turn your strings into dates, and then subtract them
      // to get a value that is either negative, positive, or zero.
      return b.start - a.start;
    });
  }
  ngOnDestroy() {
    this.employments = null;
  }

}
