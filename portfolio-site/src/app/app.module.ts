import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// MAIN PAGES
import { StartComponent } from './../pages/start/start.component';
import { ContactComponent } from './../pages/contact/contact.component';
import { SkillsComponent } from './../pages/skills/skills.component';
// SUB PAGES // SKILLS
import { CoursesComponent } from './../pages/courses/courses.component';
import { ProjectsComponent } from './../pages/projects/projects.component';
import { EmploymentsComponent } from './../pages/employments/employments.component';
import { NavigationComponent } from './navigation/navigation.component';
// Dialogs
import { CourseDialogComponent } from './dialogs/course-dialog/course-dialog.component';
import { ProjectDialogComponent } from './dialogs/project-dialog/project-dialog.component';
// Angular Material
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatToolbarModule } from '@angular/material/toolbar';

const appRoutes: Routes = [
  { path: 'start', component: StartComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'skills', component: SkillsComponent },
  { path: 'courses', component: CoursesComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'employments', component: EmploymentsComponent },
  { path: '', redirectTo: '/start', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    ContactComponent,
    SkillsComponent,
    CoursesComponent,
    ProjectsComponent,
    NavigationComponent,
    CourseDialogComponent,
    ProjectDialogComponent,
    EmploymentsComponent
  ],
  imports: [

    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false,
        useHash: true } // <-- debugging purposes only
    ),
    BrowserModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDividerModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    CourseDialogComponent,
    ProjectDialogComponent
  ]
    
  
})


export class AppModule { 
  
}
