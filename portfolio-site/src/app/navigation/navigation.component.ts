import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  private skillsActive = false;
  public name = 'MICHAELA OLAUSSON';

  constructor() { }

  ngOnInit() {
  }

  linkActivate( link ): void {
    switch ( link ) {
      case 'skills':
        this.skillsActive = true;
      break;
      default:
      this.skillsActive = false;
    }
  }
}
